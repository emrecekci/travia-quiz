import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Home from '../Screens/Home';
import Quiz from '../Screens/Quiz'
import EndPage from '../Screens/EndPage';
import Congrat from '../Screens/Congrat'
import Wrong from '../Screens/Wrong'

const screens = {
    Home  : {
        screen:Home,
        navigationOptions: {
            headerStyle: { display: "none", backgroundColor: "#8da2ad", height :100},
            cardStyle: { backgroundColor: "#ffffff" },
            headerLeft: null,
            headerShown: false,
            header: () => null
        },
    },
    Quiz : {
        screen : Quiz,
        navigationOptions: {
            headerStyle: { display: "none", backgroundColor: "#8da2ad", height :100},
            cardStyle: { backgroundColor: "#ffffff" },
            headerLeft: null,
            headerShown: false,
            header: () => null
        },
    },
    EndPage : {
        screen : EndPage,
        navigationOptions: {
            headerStyle: { display: "none", backgroundColor: "#8da2ad", height :100},
            cardStyle: { backgroundColor: "#ffffff" },
            headerLeft: null,
            headerShown: false,
            header: () => null
        },
    },
    Congrat : {
        screen : Congrat,
        navigationOptions: {
            headerStyle: { display: "none", backgroundColor: "#8da2ad", height :100},
            cardStyle: { backgroundColor: "#ffffff" },
            headerLeft: null,
            headerShown: false,
            header: () => null
        },
    },
    Wrong : {
        screen : Wrong,
        navigationOptions: {
            headerStyle: { display: "none", backgroundColor: "#8da2ad", height :100},
            cardStyle: { backgroundColor: "#ffffff" },
            headerLeft: null,
            headerShown: false,
            header: () => null
        },
    }
}

const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);