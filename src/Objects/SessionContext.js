import React from 'react'

const SessionContext = React.createContext(
  {

    general:
    {
      category: '',
      level: '',
      totalPoint: 0,
      easyScore: 0,
      mediumScore: 0,
      hardScore: 0
    },

    updateCategory: (val) => { },
    updateLevel: (val) => { },
    updateTotalPoint: (val) => { },
    updateEasyScore : (val) => { },
    updateMediumScore : (val) => { },
    updateHardScore : (val) => { },
  });

export default SessionContext;