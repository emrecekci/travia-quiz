import React, { Component } from 'react';
import { View, SafeAreaView, Alert, Text, TouchableOpacity, ActivityIndicator, TouchableNativeFeedbackBase } from 'react-native';
import SessionContext from '../Objects/SessionContext';
import { TouchableHighlight } from 'react-native-gesture-handler';
import Countdown from 'react-countdown';
//import CountDown from 'react-native-countdown-component';
import AsyncStorage from '@react-native-community/async-storage';
import * as DimensionHelper from '../utils/DimensionHelper';
import  CountdownCircleTimer  from 'react-native-countdown-circle-timer'

export default class Quiz extends Component {

    constructor(props) {
        super(props)
        this.state = { fetchData: [], index: 0, isLoading: true, fetchQuestion: [], point: 0, value: 0, easyScore: 0, mediumScore: 0, hardScore: 0, difficulty : ''};
    
    }

    randomInt = (min, max) => {
        this.setState({ value: Math.floor((max - min) * Math.random()) })
    }

    checkMaxScore = (difficulty, point, easyMaxScore, mediumMaxScore, HardMaxScore) => {


        if (difficulty === 'easy') {
            this.randomInt(1, 20)
            if (easyMaxScore < point && this.context.general.level !== '') {
                this.context.updateEasyScore(point)
            }
        }
        else if (difficulty === 'medium') {
            this.randomInt(1, 30)
            if (mediumMaxScore < point && this.context.general.level !== '') {
                this.context.updateMediumScore(point)
            }
        }
        else if (difficulty === 'hard') {
            this.randomInt(1, 40)
            if (HardMaxScore < point && this.context.general.level !== '') {
                this.context.updateHardScore(point)
            }
        }

    }


   

    checkCorrectAnswer = (correntAnswer, button, correct, difficulty) => {
      this.setState({difficulty : difficulty})
 
        if (correntAnswer === button || correntAnswer === correct) {
            if (difficulty === 'easy') {
                this.randomInt(1, 20)
            }
            else if (difficulty === 'medium') {
                this.randomInt(1, 30)
            }
            else if (difficulty === 'hard') {
                this.randomInt(1, 40)
            }
            this.setState({ isLoading: true })
            this.setState({ index: this.state.index + 1 })
            this.setState({ point: this.state.point + this.state.value })
            if (this.state.index === 9) {
                this.checkMaxScore(difficulty, this.state.point, this.state.easyScore, this.state.mediumScore, this.state.hardScore)
                this.context.updateTotalPoint(this.state.point)
                this.props.navigation.navigate('Congrat')
            }
            else {
                this.componentDidMount()
            }
        }
        else {
            this.checkMaxScore(difficulty, this.state.point, this.state.easyScore, this.state.mediumScore, this.state.hardScore)
            this.context.updateTotalPoint(this.state.point)
            this.props.navigation.navigate('Wrong')
        }

    }

   



    getData = async () => {

        try {

            const easyScoreValue = await AsyncStorage.getItem('easyScore')
            const mediumScoreValue = await AsyncStorage.getItem('mediumScore')
            const hardScoreValue = await AsyncStorage.getItem('hardScore')
            if (easyScoreValue !== null) {
                this.setState({ easyScore: easyScoreValue })
            }

            if (mediumScoreValue !== null) {
                this.setState({ mediumScore: mediumScoreValue })
                console.log('mediumScore :' + this.state.mediumScore)
            }

            if (hardScoreValue !== null) {
                this.setState({ hardScore: hardScoreValue })
                console.log('hardScore :' + this.state.hardScore)
            }

        } catch (e) {
            Alert.alert('Stroage Okunamadı', e)
            console.log("aliveli", e)
        }

    }




    finistTime = () => {
        this.context.updateTotalPoint(this.state.point)
        this.props.navigation.navigate('EndPage')
        this.checkMaxScore(this.state.difficulty, this.state.point, this.state.easyScore, this.state.mediumScore, this.state.hardScore)
    }

    async setFetchQuestion() {
        await this.setState({ fetchQuestion: [this.state.fetchData[this.state.index]] })
        this.setState({ isLoading: false })
    }

    async componentDidMount() {
        this.getData()

        try {
            const responseData = await fetch(`https://opentdb.com/api.php?amount=10&category=${this.context.general.category}&difficulty=${this.context.general.level}`);
            const responseResultData = await responseData.json();
            this.setState({ fetchData: responseResultData.results });
            await this.setFetchQuestion()

        } catch (err) {
            console.log("Error fetching data", err);
        }
    }

     renderer  ({ hours, minutes, seconds, completed }) {
        if (completed) {
          // Render a completed state
     return <Text>Time's Up</Text>
        } else {
          
          return <Text style={styles.textStyleCount}>{hours}:{minutes}:{seconds}</Text>;
        }
      };

    render() {




        return (
            <SafeAreaView>
                <View style={styles.mainView}>
                    <View style={{ flexDirection: 'row', marginTop: 50, backgroundColor: '#002686', }}>
                        <Text style={{ color: "#ffffff", fontSize: 15, fontWeight: "bold", marginTop: 20 }}>Question : {(this.state.index) + 1} /10</Text>
                  
                  

                        <Countdown
                            date={Date.now() + 15000}
                            renderer={this.renderer}
                            onComplete ={() => this.finistTime()}
                        />
                
                      

                        <Text style={{ color: "#ffffff", fontSize: 15, fontWeight: "bold", marginTop: 20,marginLeft : 70 }}>Points : {(this.state.point)}</Text>
                    </View>
                    {

                        (this.state.fetchQuestion) != undefined ?

                            this.state.fetchQuestion.map((responseData) =>
                                <View style={styles.viewStyle}>
                                    <Text style={styles.textStyle}>{responseData.question}</Text>
                                    {
                                        responseData.incorrect_answers.map((buttons) =>
                                            <TouchableOpacity onPress={() => this.checkCorrectAnswer(responseData.correct_answer, buttons, '', responseData.difficulty)} style={styles.buttonStyle} >
                                                <Text style={styles.textStyle}>{buttons}</Text>
                                            </TouchableOpacity>
                                        )
                                    }
                                    <TouchableOpacity onPress={() => this.checkCorrectAnswer(responseData.correct_answer, '', responseData.correct_answer, responseData.difficulty)} style={styles.buttonStyle} >
                                        <Text style={styles.textStyle}>{responseData.correct_answer}</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                            :
                            null
                    }
                  
                </View>
            </SafeAreaView>
        )

    }
}
Quiz.contextType = SessionContext

const styles = {
    mainView: {
        width: DimensionHelper.getWidth(375),
        height: DimensionHelper.getHeight(815),
        backgroundColor: '#0000ff',
        // justifyContent: 'center',
        //flexDirection: 'column',
        position: 'absolute'
    },
    viewStyle: {
        marginTop: 50,
    },
    buttonStyle: {
        marginTop: 10,
        alignSelf: 'center',
        backgroundColor: '#002686',
        height: 40,
        width: 200
    },
    textStyle: {
        marginTop: 10,
       // marginLeft : 20,
        alignSelf: 'center',
        color: "#ffffff",
        fontSize: 15,
        fontWeight: "bold",
    },
    textStyleCount: {
        marginTop: 20,
        marginLeft : 80,
        alignSelf: 'center',
        color: "#ffffff",
        fontSize: 15,
        fontWeight: "bold",
    }
}