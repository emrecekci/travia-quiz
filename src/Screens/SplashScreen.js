import React, { Component } from 'react';
import { View, SafeAreaView, Alert, Text, TouchableOpacity } from 'react-native';
import { Picker } from '@react-native-community/picker';
import * as DimensionHelper from '../utils/DimensionHelper';

export default class Quiz extends Component {

    render() {


        return (
            <View style={styles.viewStyles}>
                <SafeAreaView>
                    <Text style={styles.textStyles}>
                       WELCOME
            </Text>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = {
    viewStyles:
    {
        width: DimensionHelper.getWidth(375),
        height: DimensionHelper.getHeight(815),
        backgroundColor: 'orange',
      
        justifyContent: 'center',
    },
    textStyles: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        alignSelf: 'center'
    }
}