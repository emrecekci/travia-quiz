import React, { Component } from 'react';
import { View, SafeAreaView, Alert, Text, TouchableOpacity } from 'react-native';
import * as Config from '../utils/Config';
import { Picker } from '@react-native-community/picker';
import SessionContext from '../Objects/SessionContext';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from './SplashScreen'
import CountDown from 'react-native-countdown-component';


export default class Home extends Component {

    state = { categoryList: [], loading: true, level: '', selectedCategory: '',easyScore : 0,mediumScore: 0, hardScore : 0,isLoading : true,  }

    onClickQuiz = (self, selectedCategory, level) => {
        self.context.updateCategory(selectedCategory)
        self.context.updateLevel(level)
        this.props.navigation.navigate('Quiz')
    }

    getData = async () => {

        try {
    
            const easyScoreValue = await AsyncStorage.getItem('easyScore')
            const mediumScoreValue = await AsyncStorage.getItem('mediumScore')
            const hardScoreValue = await AsyncStorage.getItem('hardScore')
            console.log('maxeasyScore :' + this.state.easyScore)
            if (easyScoreValue !== null) {
              this.setState({ easyScore: easyScoreValue })  
            }
    
            if (mediumScoreValue !== null) {
                this.setState({ mediumScore: mediumScoreValue })
                console.log('mediumScore :' + this.state.mediumScore)
              }
    
              if (hardScoreValue !== null) {
                this.setState({ hardScore: hardScoreValue })
                console.log('hardScore :' + this.state.hardScore)
              }
              this.setState({loaded : false})
    
        } catch (e) {
          Alert.alert('Stroage Okunamadı',e)
          console.log("aliveli", e)
        }
    
      }

      onLogOut = async () => {
        try {
          this.setState({ easyScore: " " })
          this.setState({ mediumScore: " " })
          this.setState({ hardScore: " " })
          await AsyncStorage.clear()
        }
        catch (err) {
          Alert.alert(err)
        }
      }

    async componentDidMount() {
        this.getData()
        try {
            const categoryJson = await fetch(Config.CATEGORY);
            const category = await categoryJson.json();
            this.setState({ categoryList: category.trivia_categories });
            this.setState({isLoading : false})
        } catch (err) {
            console.log("Error fetching data", err);
        }
    }

    categoryPicker = () => {
        return (<Picker
            selectedValue={this.state.selectedCategory}
            style={{ height: 30, width: 350, alignSelf: 'center' }}
            onValueChange={(itemValue, itemIndex) =>
                this.setState({ selectedCategory: itemValue })
            }>
            {this.state.categoryList.map((responseData) =>
                <Picker.Item label={(responseData.name)} value={responseData.id} />
            )}
        </Picker>)
    }


    async shouldComponentUpdate () {
      //this.getData()
    }




    render() {
        if (this.state.isLoading) {
            return <SplashScreen />;
          }

        return (
            <SafeAreaView >
                <View >
                    <View>
                        {this.categoryPicker()}
                    </View>
                    <View style={{ marginTop: 90 }}>
                        <Picker
                            selectedValue={this.state.level}
                            style={{ height: 30, width: 350, alignSelf: 'center', marginTop: 70 }}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({ level: itemValue })
                            }>
                            <Picker.Item label="Easy" value="easy" />
                            <Picker.Item label="Medium" value="medium" />
                            <Picker.Item label="Hard" value="hard" />
                        </Picker>
                    </View>
                    <View style={{ marginTop: 200, alignSelf: 'center' }}>
                        <TouchableOpacity onPress={() => this.onClickQuiz(this, this.state.selectedCategory, this.state.level)} style={{ backgroundColor: '#0000FF', height: 40, width: 200 }}  >
                            <Text style={styles.buttonTextStyle}>GET STARTED</Text>
                        </TouchableOpacity>
                        {
                            (this.state.loaded) ? null :
                            <View>
                        <Text style={styles.textStyle}>Easy : {this.state.easyScore}</Text>
                        <Text style={styles.textStyle}>Medium : {this.state.mediumScore}</Text>
                        <Text style={styles.textStyle}>Hard : {this.state.hardScore}</Text>
                        </View>
                        }
                         <TouchableOpacity onPress={() => this.onLogOut()} style={{ backgroundColor: '#0000FF', height: 40, width: 200 }}  >
                            <Text style={styles.buttonTextStyle}>CLEAR DATA</Text>
                        </TouchableOpacity>

                      
                    </View>
                </View>
               
            </SafeAreaView>
        )
    }
}
Home.contextType = SessionContext
const styles = {
    buttonTextStyle: {
        marginTop: 7,
        alignSelf: 'center',
        color: "#ffffff",
        fontSize: 18,
        fontWeight: "bold",
    },
    textStyle: {
        marginTop: 7,
        alignSelf: 'center',
        fontSize: 18,
        fontWeight: "bold",
    },
}