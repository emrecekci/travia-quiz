import React, { Component } from 'react';
import { View, SafeAreaView, Alert, Text, TouchableOpacity } from 'react-native';
import * as Config from '../utils/Config';
import { Picker } from '@react-native-community/picker';
import SessionContext from '../Objects/SessionContext';
import * as DimensionHelper from '../utils/DimensionHelper';

export default class EndPage extends Component {

    constructor(props) {
        super(props)
        console.log(this.props)

    }


    render() {

        return (
            <SafeAreaView>
                <View style={{ position: 'absolute' }}>
                    <View style={styles.mainView}>
                        <Text style={styles.firtsttextStyle}>TIME'S UP</Text>
                        <Text style={styles.secondtextStyle}>You are late, time’s up.</Text>
                        <Text style={styles.secondtextStyle}>{this.context.general.totalPoint}</Text>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')} style={styles.buttonStyle} >
                            <Text style={styles.textStyle}>Main Menu</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

EndPage.contextType = SessionContext

const styles = {
    mainView: {
        width:  DimensionHelper.getWidth(375),
        height: DimensionHelper.getHeight(815),
        backgroundColor: '#908f8f',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    firtsttextStyle: {
        alignSelf: 'center',
        color: "#ffffff",
        fontSize: 25,
        fontWeight: "bold",
    },
    secondtextStyle: {
        marginTop: 15,
        alignSelf: 'center',
        color: "#ffffff",
        fontSize: 20,
        fontWeight: "bold",
    },
    buttonStyle: {
        marginTop: 15,
        alignSelf: 'center',
        backgroundColor: '#783c00',
        height: 50,
        width: 280
    },
    textStyle: {
        marginTop: 12,
        alignSelf: 'center',
        color: "#ffffff",
        fontSize: 20,
        fontWeight: "bold",
    }
}