import {Dimensions} from 'react-native'

export function getHeight(height)
{
    var deviceHeight = Dimensions.get('window').height
    return (height / 812) * deviceHeight
}

export function getWidth(width)
{
    var deviceWidth = Dimensions.get('window').width
    return (width / 375) * deviceWidth
}