import React, { Component } from 'react';
import Navigator from './src/routes/HomeStack'
import SessionContext from './src/Objects/SessionContext'
import AsyncStorage from '@react-native-community/async-storage';

export default class App extends Component {
  constructor(props){
    super(props)
  this.state = {general:
    {
      category: '',
      level : '',
      totalPoint : 0,
      easyScore: 0,
      mediumScore: 0,
      hardScore : 0
    },
    updateCategory: this.updateCategory.bind(this),
    updateLevel: this.updateLevel.bind(this),
    updateTotalPoint : this.updateTotalPoint.bind(this),
    updateEasyScore : this.updateEasyScore.bind(this),
    updateMediumScore : this.updateMediumScore.bind(this),
    updateHardScore : this.updateHardScore.bind(this)
  }}

  updateCategory = async (val) => {
    this.setState(({ general }) => ({
      general: {
        ...general,
        category: val
      }
    }))
  }

  updateLevel = async (val) => {
    this.setState(({ general }) => ({
      general: {
        ...general,
        level: val,
      }
    }))
  }

  updateTotalPoint = async (val) => {
    this.setState(({ general }) => ({
      general: {
        ...general,
        totalPoint: val,
      }
    }))
  }

  updateEasyScore = async (val) => {
    this.setState(({ general }) => ({
      general: {
        ...general,
        easyScore: val
      }
    }))
    await AsyncStorage.setItem('easyScore', JSON.stringify(parseInt(val)))
  }

  updateMediumScore = async (val) => {
    this.setState(({ general }) => ({
      general: {
        ...general,
        mediumScore: val
      }
    }))
    await AsyncStorage.setItem('mediumScore', JSON.stringify(parseInt(val)))
  }

  updateHardScore = async (val) => {
    this.setState(({ general }) => ({
      general: {
        ...general,
        hardScore: val
      }
    }))
    await AsyncStorage.setItem('hardScore', JSON.stringify(parseInt(val)))
  }

  render() {
    return (
      <SessionContext.Provider value={this.state}>


        <Navigator></Navigator>
      </SessionContext.Provider>

    )
  }
}

//export default App;
